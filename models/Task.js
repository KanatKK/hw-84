const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const taskSchema = new Schema({
    user: {
        type: String,
        required: true
    },
    title: {
      type: String,
      required: true
    },
    description: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ["new", "in_progress", "done"],
        default: "new",
    }
});

taskSchema.methods.generateUser = function (user) {
    this.user = user;
};

const Task = mongoose.model("Task", taskSchema);
module.exports = Task;