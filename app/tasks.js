const router = require("express").Router();
const auth = require("../middleware/auth");
const Task = require("../models/Task");

router.get("/", auth, async (req, res) => {
    try {
        const users = await Task.find({"user": req.user.token});
        res.send(users);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", auth, async (req, res) => {
    try {
        const task = new Task(req.body);
        task.generateUser(req.user.token);
        await task.save();
        res.send(task);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.delete("/:id", auth, async (req, res) => {
    const result = await Task.find({"_id": req.params.id});
    if (result[0].user === req.user.token) {
        try {
            await Task.findByIdAndDelete(req.params.id);
            res.send("Deleted");
        } catch (e) {
            return res.status(400).send(e);
        }
    } else {
        res.send("Wrong token");
    }
});

router.put("/:id", auth, async (req, res) => {
    const result = await Task.find({"_id": req.params.id});
    if (result[0].user === req.user.token) {
        try {
            if (req.body.user) {
                res.status(401).send("Can not change user");
            } else {
                await Task.findByIdAndUpdate(req.params.id, req.body);
                const task = await Task.find({"_id": req.params.id});
                res.send(task);
            }
        } catch (e) {
            return res.status(400).send(e);
        }
    } else {
        res.send("Wrong token");
    }
});

module.exports = router;