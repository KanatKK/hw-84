const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const user = require("./app/users");
const task = require("./app/tasks");
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
  await mongoose.connect("mongodb://localhost/toDoList", {useNewUrlParser: true});

  app.use("/users", user);
  app.use("/tasks", task);

  console.log("Connected to mongo DB");

  app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
  });
};

run().catch(console.log);